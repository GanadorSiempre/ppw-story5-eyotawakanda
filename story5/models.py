from django.db import models

class MatKul(models.Model):
    name = models.CharField(max_length=50)
    lecturer = models.CharField(max_length=50)
    sks = models.PositiveSmallIntegerField()
    description = models.TextField()
    semester = models.PositiveIntegerField()
    room = models.CharField(max_length=20)

    def __str__(self):
        return self.judul

# Create your models here.
