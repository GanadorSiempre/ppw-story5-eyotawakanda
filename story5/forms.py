from django import forms
from .models import MatKul

class MataKuliah(forms.ModelForm):
    class Meta:
        model = MatKul
        fields = ['name', 'lecturer', 'sks', 'description', 'semester', 'room']