from django.contrib import admin
from django.urls import path
from story5 import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.form, name='form'),
    path('table/<int:id>', views.table, name='table'),
    path('add/', views.addMatkul, name='addMatkul'),
    path('delete/<int:id>', views.byeMatkul, name='byeMatkul'),
]