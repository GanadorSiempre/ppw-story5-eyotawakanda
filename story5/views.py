from django.shortcuts import render, redirect
from .models import MatKul
from .forms import MataKuliah

def form (request):
    obj = MataKuliah.objects.all()
    context = {
        'obj' : obj
    }
    return render(request, "form.html", context)

def table (request, id):
    obj = MataKuliah.Objects.get(id=id)
    context = {
        'obj' : obj
    }
    return render(request, "table.html")

def addMatkul (request):
    obj = MatKul(request.POST or None)
    if (obj.is_valid() and request.method == 'POST'):
        obj.save()
        return redirect('form')

    context = {
        'obj' : obj
    }
    return render(request, "add.html", context)

def byeMatkul (request, id):
    obj = MataKuliah.objects.get(id=id)
    obj.delete()
    return redirect('form')

# Create your views here.
